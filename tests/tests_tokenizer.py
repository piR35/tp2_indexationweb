from index.opener import openFile
from index.extracter import extractTitles
from index.tokenizer import tokenizeTitles

def testTokenizeTitles(jsonfile):
    #GIVEN
    data = openFile(jsonfile)
    titles = extractTitles(data)
    index = tokenizeTitles(titles)
    #WHEN
    #THEN
    assert len(index) >= len(titles)

if __name__ == "__main__":
    testTokenizeTitles("crawled_urls.json")
    print("test passed")