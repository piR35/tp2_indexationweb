from index.opener import openFile

def testOpenFile(jsonfile):
    #GIVEN
    openfile = openFile(jsonfile)
    #WHEN
    #THEN
    return isinstance(openfile, list)

if __name__ == "__main__":
    testOpenFile("crawled_urls.json")
    print("test passed")