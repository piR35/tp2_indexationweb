from index.opener import openFile
from index.extracter import extractTitles

def testExtractTitles(jsonfile):
    #GIVEN
    data = openFile(jsonfile)
    titles = extractTitles(data)
    #WHEN
    title1 = titles[0]
    #THEN
    return isinstance(title1, str)

if __name__ == "__main__":
    testExtractTitles("crawled_urls.json")
    print("test passed")