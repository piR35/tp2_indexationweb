import json

from index.opener import openFile
from index.extracter import extractTitles
from index.tokenizer import tokenizeTitles

def main(crawl):

    data = openFile(crawl)

    titles = extractTitles(data)

    index = tokenizeTitles(titles)

    index_set = set(index)

    with open("title.non_pos_index.json", "w") as f:
        json.dump(index_set, f)

    nb_docs = len(data)
    nb_tokens = len(index)

    statistics = {"num docs": nb_docs, "num tokens": nb_tokens, "avg tokens per docs": nb_tokens/nb_docs}

    with open("metadata.json", "w") as f:
        json.dump(statistics, f)


if __name__ == "__main__":
    main("crawled_urls.json")