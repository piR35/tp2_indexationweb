from nltk.tokenize import word_tokenize

def tokenizeTitles(list):
    l = []
    for title in list:
        tokens = word_tokenize(title)
        for token in tokens:
            l.append(token)
    return l