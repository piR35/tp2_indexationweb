from bs4 import BeautifulSoup
import urllib.request

def extractTitles(file):
    list = []
    for url in file:
        html_page = urllib.request.urlopen(url)
        soup = BeautifulSoup(html_page, "html.parser")
        title = soup.title.string
        list.append(title)
    return list