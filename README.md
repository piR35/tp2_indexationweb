# INDEX EN PYTHON

Contributeur : Pierre Vasseur

## Explication du code

opener.py sert à créer une instance python en liste d'un fichier au format json

extracter.py permet d'extraire tous les titres à partir d'une liste d'urls fournies en entrée

tokenizer.py a pour rôles de tokenizer un ensemble de titres donnés dans une liste, et de retourner justement la liste de ces tokens


## Execution

il suffit de lancer le fichier main.py pour créer un index à partir du fichier crawled_urls.json appelé "title.non_pos_index.json"
un fichier json appelé "metadata.json" est également créé par la même exécution et fournit différentes statistiques sur les documents.